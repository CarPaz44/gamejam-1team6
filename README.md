# GameJam-1Team6
CPI 211 Roll-A-Ball Game Jam Spring 2019
Please be sure to make any commits to a feature branch before moving them to master

? denotes things that are not core but can be implemented if we have time

TODO
Assests;
-Music(music obtianed, needs implementing)
  -track for start screen
  -track for victory
  -track for game over
  -1-3 tracks for during level
-Skins
  -Ball
  -Walls
  -Regular floor
  -kill floor
  ? additional floors
-Art
  ?art for start screen
  ?art for victory screen
  ?art for gameover screen
Mechanics; 
-Ball Jump
-Floor that kills
-OOBs reset
UI;
-Score
?level timer 
Design;
-Start Screen
-level
  -first area focused on teaching jumping
  -second area focused on teaching kill floor
  -third area combining both 
-Pickup placements
-Victory condition/victory screen
-Loss condition/gameover screen
