﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    public float speed;
    public Text scoreText;
    private Rigidbody myRigidBody;
    private int scoreCount;
	// Use this for initialization
	void Start () {
        scoreCount = 0;
        scoreText.text = "Score:" + scoreCount;
        myRigidBody = GetComponent<Rigidbody>();
	}

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        myRigidBody.AddForce(movement * speed);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            scoreCount = scoreCount + 1;
            scoreText.text = "Score: " + scoreCount;
            other.gameObject.SetActive(false);
        }
        
    }

    // Update is called once per frame
    void Update () {
		
	}
}
